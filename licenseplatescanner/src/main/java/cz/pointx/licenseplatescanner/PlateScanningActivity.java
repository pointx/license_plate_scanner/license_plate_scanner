package cz.pointx.licenseplatescanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;

import com.openalpr.jni.AlprCoordinate;
import com.openalpr.jni.AlprResults;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.pointx.licenseplatescanner.alpr.PlateRecognizer;
import cz.pointx.licenseplatescanner.view.CameraFrameCoordinates;
import cz.pointx.licenseplatescanner.view.PlateListAdapter;
import cz.pointx.licenseplatescanner.view.RegionOfInterestView;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.view.CameraView;

import static io.fotoapparat.log.LoggersKt.fileLogger;
import static io.fotoapparat.log.LoggersKt.logcat;
import static io.fotoapparat.log.LoggersKt.loggers;
import static io.fotoapparat.selector.FlashSelectorsKt.autoFlash;
import static io.fotoapparat.selector.FlashSelectorsKt.autoRedEye;
import static io.fotoapparat.selector.FlashSelectorsKt.torch;
import static io.fotoapparat.selector.FocusModeSelectorsKt.autoFocus;
import static io.fotoapparat.selector.FocusModeSelectorsKt.continuousFocusPicture;
import static io.fotoapparat.selector.FocusModeSelectorsKt.continuousFocusVideo;
import static io.fotoapparat.selector.FocusModeSelectorsKt.fixed;
import static io.fotoapparat.selector.LensPositionSelectorsKt.back;
import static io.fotoapparat.selector.ResolutionSelectorsKt.highestResolution;
import static io.fotoapparat.selector.SelectorsKt.firstAvailable;

/**
 * Activity that recognizes license plates (czech abbr.: SPZ) from camera images.
 */
public class PlateScanningActivity extends Activity implements FrameProcessor, PlateRecognizer.iRecognizeResultCallback
{
    private final String TAG = "SpzCameraActivity";

    // Request code for starActivityForResult(...)
    public static final int REQUEST_PLATE_SCAN = 1;

    // Key to the result Intent data
    public static final String PLATES_RESULT = "PlatesResult";

    // View where the preview images are rendered
    CameraView cameraView;

    // Fotoapparat's main class for camera controlling
    Fotoapparat fotoaparat;

    // License plate recognition from camera images using OpenALPR
    PlateRecognizer plateRecognizer;

    // View that draws a rectangle around the subimage where the scanning is performed
    RegionOfInterestView roiView;

    // List of found plates (strings -> adapter -> view)
    ArrayList<String> plates = new ArrayList<>();
    PlateListAdapter plateAdapter;
    ListView plateListView;

    // Buttons
    Button btnOk;
    Button btnCancel;


    // Load C++ libraries
    static
    {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("openalpr_jni");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        restoreInstanceState(savedInstanceState);

        initViews();
        initFotoapparat();

        boolean alprOK = initPlateRecognizer();
        if(!alprOK)
            alprAssetsError();
    }


    private void initViews()
    {
        // Title is disturbing for camera preview
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Enable the same aspect-ratio as in camera (without manual cropping)
        int fullscreen = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setFlags(fullscreen, fullscreen);

        setContentView(R.layout.activity_spz_camera);
        cameraView = (CameraView) findViewById(R.id.cameraView);

        roiView = (RegionOfInterestView) findViewById(R.id.regionOfInterestView);
        roiView.setStrokeWidthPx(dpToPx(2));

        plateAdapter = new PlateListAdapter(this, R.layout.plate_list_item, R.id.lblPlate, plates);
        plateListView = (ListView) findViewById(R.id.plateListView);
        plateListView.setAdapter(plateAdapter);

        btnOk = (Button) findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finishWithResults();
            }
        });

        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }


    private void initFotoapparat()
    {
        // Init Fotoapparat
        fotoaparat = Fotoapparat
            .with(this)
            .into(cameraView)                        // view which will draw the camera preview
            .previewScaleType(ScaleType.CenterCrop)  // we want the preview to fill the view
            .photoResolution(highestResolution())    // we want to have the biggest photo possible
            .lensPosition(back())                    // we want back camera
            .focusMode(firstAvailable(               // (optional) use the first focus mode which is supported by device
                continuousFocusPicture(),
                continuousFocusVideo(),
                autoFocus(),                         // in case if continuous focus is not available on device, auto focus will be used
                fixed()                              // if even auto focus is not available - fixed focus mode will be used
            ))
            .flash(firstAvailable(                   // (optional) similar to how it is done for focus mode, this time for flash
                autoRedEye(),
                autoFlash(),
                torch()
            ))
            .frameProcessor(this)                    // (optional) receives each frame from preview stream
            .logger(loggers(                         // (optional) we want to log camera events in 2 places at once
                logcat(),                            // ... in logcat
                fileLogger(this)             // ... and to file
            ))
            .build();
    }


    boolean initPlateRecognizer()
    {
        String androidDataDir = getExternalFilesDir();
        if(androidDataDir == null)
            return false;

        plateRecognizer = new PlateRecognizer(this, androidDataDir, this);
        boolean loaded = plateRecognizer.isLoaded();
        return loaded;
    }


    /**
     * Get full path to external app directory
     * @return path to a directory or null (in case of error)
     */
    private String getExternalFilesDir()
    {
        File externalDirs = getExternalFilesDir(null);
        if(externalDirs == null)
            return null;

        String externalFilesDir;
        try {
            externalFilesDir = externalDirs.getCanonicalPath();
        }
        catch (IOException e) {
            e.printStackTrace();
            externalFilesDir = null;
        }

        return externalFilesDir;
    }


    /**
     * Show dialog in case the OpenALPR assets could not be copied into external storage.
     */
    private void alprAssetsError()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.asset_error);
        builder.setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id)
            {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     * Find license plates in a single image frame asynchronously.
     * This method runs in background Fotoapparat's thread.
     */
    @Override
    public void process(Frame frame)
    {
        final int rotation = frame.getRotation();
        final byte[] jpg = frameToJpg(frame);
        plateRecognizer.postFrame(jpg, rotation);
    }


    /**
     * Process found plates
     */
    private void processResult(AlprResults results)
    {
        List<Rect> plateRects = getPlateRects(results);
        roiView.setPlateRects(plateRects);
        boolean newPlateAdded = false;

        for(int i=0; i<results.getPlates().size(); i++)
        {
            String plate = results.getPlates().get(i).getBestPlate().getCharacters();
            if(!plates.contains(plate)) // discard already scanned plates
            {
                plates.add(0, plate);
                plateAdapter.notifyDataSetChanged();
                newPlateAdded = true;
            }
        }

        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (vibrator != null && newPlateAdded)
            vibrator.vibrate(100);
    }


    /**
     * Convert AlprCoordinate to Rect objects for rendering them on screen.
     */
    private List<Rect> getPlateRects(AlprResults results)
    {
        List<Rect> plateRects = new ArrayList<>();
        for(int i=0; i<results.getPlates().size(); i++)
        {
            List<AlprCoordinate> coordinates = results.getPlates().get(i).getPlatePoints();
            int left = coordinates.get(0).getX();
            int top = coordinates.get(0).getY();
            int right = coordinates.get(2).getX();
            int bottom = coordinates.get(2).getY();
            Rect rect = new Rect(left, top, right, bottom);
            plateRects.add(rect);
        }

        return plateRects;
    }


    /**
     * Convert a Fotoapparat's Frame to JPG byte array.
     */
    private byte[] frameToJpg(Frame frame)
    {
        int width = frame.getSize().width;
        int height = frame.getSize().height;
        int rotation = frame.getRotation();

        Rect regionOfInterest = CameraFrameCoordinates.alprRegionOfInterest(width, height, rotation);

        YuvImage yuvImage = new YuvImage(frame.getImage(), ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(regionOfInterest, 80, os);
        return os.toByteArray();
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        fotoaparat.start();
        plateRecognizer.start();
    }


    @Override
    protected void onPause()
    {
        super.onPause();

        //TODO: broken stopping: https://github.com/RedApparat/Fotoapparat/issues/191
        // fixed until resolved by adding 300ms sleep
        fotoaparat.stop();
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        plateRecognizer.stop();
    }


    /**
     * Save scanned plates before device rotates and activity is restarted
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putStringArrayList("plates", plates);
    }


    /**
     * Restore scanned plates after activity restart
     */
    public void restoreInstanceState(Bundle savedInstanceState)
    {
        if(savedInstanceState == null)
            return;

        ArrayList<String> platesList = savedInstanceState.getStringArrayList("plates");
        if(platesList != null)
            plates = platesList;
    }


    /**
     * iRecognizeResultCallback implementation.
     * Processes OpenALPR recognize results. Called from background thread.
     */
    @Override
    public void onPlateResults(final AlprResults results)
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                processResult(results);
            }
        });
    }


    /**
     * Finish Activity and return scanned plates
     */
    private void finishWithResults()
    {
        Intent resultData = new Intent();
        String[] platesArray = plates.toArray(new String[0]); // https://stackoverflow.com/a/4042464
        resultData.putExtra(PLATES_RESULT, platesArray);
        setResult(RESULT_OK, resultData);
        finish();
    }


    /**
     * Convert density-independent pixels to pixels
     */
    int dpToPx(float dp)
    {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int unit = TypedValue.COMPLEX_UNIT_DIP;
        int pixels = (int) TypedValue.applyDimension(unit, dp, metrics);
        return pixels;
    }
}
