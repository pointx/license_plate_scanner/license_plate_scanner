package cz.pointx.licenseplatescanner.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import java.util.List;

import cz.pointx.licenseplatescanner.R;

/**
 * String list adapter that shows scanned license plates and allows each plate to be deleted.
 */
public class PlateListAdapter extends ArrayAdapter<String>
{


    public PlateListAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<String> objects)
    {
        super(context, resource, textViewResourceId, objects);
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        View view = super.getView(position, convertView, parent);

        ImageView btnDelete = (ImageView) view.findViewById(R.id.btnDelete);
        btnDelete.setClickable(true);
        btnDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                remove(getItem(position));
            }
        });

        return view;
    }
}
