package cz.pointx.licenseplatescanner.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.List;


/**
 * This view represents a layer above the camera frames. It draws cropped region where the plates
 * are searched (hence the name - we are interested in this region only). When plates are found
 * their rectangles are also drawn.
 */
public class RegionOfInterestView extends View
{
    private final String TAG = "RegionOfInterestView";

    // Drawing styles
    Paint roiPaint = new Paint();
    Paint platePaint = new Paint();

    // Region of interest
    Rect roi = new Rect();

    // Debug - bitmap of the region of interest
    Bitmap roiBitmap;

    // Found plates
    List<Rect> plateRects;


    public RegionOfInterestView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        initPaints();
    }


    /**
     * Customize the width of the rectangle
     */
    public void setStrokeWidthPx(int strokeWidthPx)
    {
        roiPaint.setStrokeWidth(strokeWidthPx);
        platePaint.setStrokeWidth(strokeWidthPx * 0.66f);
        invalidate();
    }


    /**
     * Not called each frame - to force redrawing, call invalidate().
     */
    @Override
    public void onDraw(Canvas canvas)
    {
        if(roi.width() == 0)
            roi = CameraFrameCoordinates.alprRegionOfInterest(getWidth(), getHeight());

        canvas.drawRect(roi.left, roi.top, roi.right, roi.bottom, roiPaint);

        if(roiBitmap != null)
            canvas.drawBitmap(roiBitmap, 10, 10, null);

        if(plateRects != null)
            drawPlateRects(canvas);
    }


    private void drawPlateRects(Canvas canvas)
    {
        for(int i=0; i<plateRects.size(); i++)
        {
            canvas.drawRect(plateRects.get(i), platePaint);
        }
    }


    /**
     * Debug - draw JPG
     */
    public void debugDrawROI(byte[] jpeg)
    {
        invalidate();

        if(roiBitmap != null)
            roiBitmap.recycle();

        roiBitmap = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);
    }


    private void initPaints()
    {
        setStrokeWidthPx(5);

        roiPaint.setColor(Color.WHITE);
        roiPaint.setStyle(Paint.Style.STROKE);

        platePaint.setColor(Color.GREEN);
        platePaint.setStyle(Paint.Style.STROKE);
    }


    public void setPlateRects(List<Rect> plateRects)
    {
        for(int i=0; i<plateRects.size(); i++)
        {
            Rect plate = plateRects.get(i);
            plate.offset(roi.left, roi.top);
        }

        this.plateRects = plateRects;
        invalidate();
    }
}