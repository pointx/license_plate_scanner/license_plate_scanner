package cz.pointx.licenseplatescanner.view;

import android.graphics.Rect;

/**
 * Static methods for handling Android issues with Camera frames - they are retrieved from hardware
 * camera with fixed orientation (usually landscape), but it is necessary to work with them
 * according to actual device orientation, so the coordinates have to be converted.
 */
public class CameraFrameCoordinates
{
    /**
     * Get a cropped rectangle of the image - due to low performance of mobile devices, only a
     * subimage has to be scanned for license plates.
     *
     * @return rectangle of the subimage to be scanned (orientation-independent)
     */
    public static Rect alprRegionOfInterest(int width, int height)
    {
        double roiWidth, roiHeight;
        int left, top, right, bottom;

        // Landscape
        if(width > height)
        {
            // On 4:3 displays height*0.8 could overlap to plate list.
            roiWidth = Math.min(height * 0.8, width/2 * 0.9);
            roiHeight = roiWidth / 4;

            left = (int) (width/4 - roiWidth/2);
            top = (int) (height/2 - roiHeight/2);
        }

        // Portrait
        else
        {
            roiWidth = width * 0.8;
            roiHeight = roiWidth / 4;
            left = (int) (width/2 - roiWidth/2);
            top = (int) (height/4 - roiHeight/2);
        }

        right = (int) (left + roiWidth);
        bottom = (int) (top + roiHeight);

        Rect rect = new Rect(left, top, right, bottom);
        return rect;
    }


    /**
     * Get the subimage rectangle of the native frame (fixed orientation), but count with device
     * orientation - required for proper display drawing.
     */
    @SuppressWarnings("SuspiciousNameCombination")
    public static Rect alprRegionOfInterest(int width, int height, int rotation)
    {
        if(rotation == 0)
        {
            Rect roi = alprRegionOfInterest(width, height);
            return roi;
        }

        else if(rotation == 180)
        {
            Rect roi = alprRegionOfInterest(width, height);
            roi.left += width/2;
            roi.right += width/2;
            return roi;
        }

        // Rotation 270
        else
        {
            Rect roi = alprRegionOfInterest(height, width);

            int roiWidth = roi.right - roi.left;
            int roiHeight = roi.bottom - roi.top;

            int roiLeft = roi.left;
            roi.left = roi.top;
            roi.top = roiLeft;
            roi.right = roi.left + roiHeight;
            roi.bottom = roi.top + roiWidth;
            return roi;
        }
    }
}
