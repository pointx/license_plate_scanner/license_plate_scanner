package cz.pointx.licenseplatescanner.alpr;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Function that handle OpenALPR assets.
 */
public class AlprAssets
{


    /**
     * Copy OpenALPR assets to external directory in order to allow access from native code.
     * @return true if the copying was successful, false otherwise
     */
    public static boolean copyAssets(Context context, String openAlprRuntimeData)
    {
        assert openAlprRuntimeData != null;

        boolean copied = AlprAssets.copyAssetFolder(
            context.getAssets(),
            "runtime_data",
            openAlprRuntimeData
        );

        return copied;
    }


    /**
     * Copies the assets folder.
     *
     * @param assetManager The assets manager.
     * @param fromAssetPath The from assets path.
     * @param toPath The to assets path.
     *
     * @return A boolean indicating if the process went as expected.
     */
    public static boolean copyAssetFolder(AssetManager assetManager, String fromAssetPath, String toPath) {
        try {

            String[] files = assetManager.list(fromAssetPath);

            //noinspection ResultOfMethodCallIgnored
            new File(toPath).mkdirs();

            boolean res = true;

            for (String file : files)
            {
                String fromFilePath = fromAssetPath + "/" + file;
                String toFilePath = toPath + "/" + file;

                // Skip already existing files
                if(new File(toPath + "/" + file).exists())
                    continue;

                if (file.contains("."))
                    res &= copyAsset(assetManager, fromFilePath, toFilePath);
                else
                    res &= copyAssetFolder(assetManager, file, toFilePath);
            }

            return res;
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * Copies an asset to the application folder.
     *
     * @param assetManager The asset manager.
     * @param fromAssetPath The from assets path.
     * @param toPath The to assests path.
     *
     * @return A boolean indicating if the process went as expected.
     */
    private static boolean copyAsset(AssetManager assetManager, String fromAssetPath, String toPath)
    {
        InputStream in;
        OutputStream out;

        try {
            in = assetManager.open(fromAssetPath);

            boolean created = new File(toPath).createNewFile();
            if(!created)
                return false;

            out = new FileOutputStream(toPath);

            copyFile(in, out);
            in.close();
            out.flush();
            out.close();
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Copies a file.
     *
     * @param in The input stream.
     * @param out The output stream.
     *
     * @throws IOException
     */
    private static void copyFile(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];

        int read;

        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

}
