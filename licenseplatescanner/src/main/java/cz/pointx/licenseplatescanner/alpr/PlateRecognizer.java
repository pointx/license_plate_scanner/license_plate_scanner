package cz.pointx.licenseplatescanner.alpr;

import android.content.Context;
import android.util.Log;

import com.openalpr.jni.AlprPlate;
import com.openalpr.jni.AlprResults;
import com.openalpr.jni.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;


/**
 * Class responsible for license plate recognition using OpenALPR.
 */
public class PlateRecognizer
{
    private static final String TAG = "PlateRecognizer";

    // Maximum time of a single scan. Sometimes a scaning lasts extremely long (up to 40s) so after
    // specified time, the scanning is discarded.
    private static final long MAX_SCAN_TIME_MS = 2000;

    /**
     * Asynchronous callback called after scanning is finished.
     */
    public interface iRecognizeResultCallback
    {
        void onPlateResults(AlprResults results);
    }

    /**
     * Structure that holds a jpg frame with its orientation. Used for enqueuing before recognition
     * starts.
     */
    private class JpgFrame
    {
        JpgFrame(byte[] jpg, int rotation)
        {
            this.jpg = jpg;
            this.rotation = rotation;
        }

        public byte[] jpg;
        public int rotation;
    }

    // If the alpr is properly initialized
    private boolean isLoaded = false;

    // Path to OpenALPR runtime_data folder
    private String runtimeDataPath;

    // List of plates found in the previous frame (to verify its a valid plate)
    private List<String> lastPlates = new ArrayList<>();

    // Result asynchronous callback
    private iRecognizeResultCallback callback;

    // Queue for camera frames
    private ArrayBlockingQueue<JpgFrame> frameQueue = new ArrayBlockingQueue<>(1);

    // Thread that runs OpenALPR
    private Thread recognizeThread;

    // Time when the actual scanning begun.
    private long lastResultTime = System.currentTimeMillis();
    private int threadResets = 0;

    public PlateRecognizer(Context context, String externalDir, iRecognizeResultCallback callback)
    {
        this.callback = callback;
        runtimeDataPath = externalDir + File.separatorChar + "runtime_data";
        isLoaded = AlprAssets.copyAssets(context, runtimeDataPath);
    }


    public boolean isLoaded()
    {
        return isLoaded;
    }


    /**
     * Start OpenALPR scanning. After calling this method, frames can be posted using postFrame(...)
     * method.
     */
    public void start()
    {
        if(callback == null) // No reason to work when nobody is interested in results.
            return;

        startAlpr_native(runtimeDataPath);
        startRecognizeThread();
    }


    /**
     * Stop OpenALPR scanning. After calling this method, no more frames should be posted.
     */
    public void stop()
    {
        stopRecognizeThread();
    }


    /**
     * Start a new thread that will perform actual OpenALPR recognition.
     */
    private void startRecognizeThread()
    {
        recognizeThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                final BlockingQueue<JpgFrame> queue = frameQueue;
                recognizeFrame(queue);
            }
        });
        recognizeThread.start();
    }


    /**
     * Stop OpenALPR scanning thread.
     */
    private void stopRecognizeThread()
    {
        recognizeThread = null;

        // this works as a flag for actually running thread. No need for atomicity, the thread
        // just stops 'sometimes'.
        threadResets++;
    }


    /**
     * Recognize thread loop function.
     */
    private void recognizeFrame(BlockingQueue<JpgFrame> queue)
    {
        // prepare flag for stopping
        int threadResetsBefore = threadResets;

        while(true)
        {
            JpgFrame frame = null;

            // Take a frame
            try { frame = queue.poll(2*MAX_SCAN_TIME_MS, TimeUnit.MILLISECONDS); }
            catch (InterruptedException e) { e.printStackTrace(); }

            // Check if the scanning is too long
            if(frame == null || threadResetsBefore != threadResets)
            {
                Log.v(TAG, "Thread " + Thread.currentThread().getId() + " no more needed.");
                break;
            }

            // Perform recognition
            AlprResults results = recognize(frame.jpg, frame.rotation);
            lastResultTime = System.currentTimeMillis();
            callback.onPlateResults(results);
        }
    }


    /**
     * Prepare a camera frame for asynchronous recognition
     * @param jpgBytes JPG image to be scanned
     * @param rotation frame rotation according to Fotoapparat's PreviewFrame class
     */
    public void postFrame(byte[] jpgBytes, int rotation)
    {
        JpgFrame frame = new JpgFrame(jpgBytes, rotation);

        long now = System.currentTimeMillis();
        boolean scanningTooLong = (now - lastResultTime) > MAX_SCAN_TIME_MS;

        if(scanningTooLong)
        {
            Log.w(TAG, "Scanning lasts too long. Resetting.");
            stop();
            start();
            lastResultTime = System.currentTimeMillis();
        }

        frameQueue.clear();
        frameQueue.add(frame);
    }


    /**
     * Recognize jpeg image with specified rotation (from Fotoapparat's Frame)
     */
    private AlprResults recognize(byte[] jpgBytes, int rotation)
    {
        String stringResults = recognize_native(jpgBytes, jpgBytes.length, rotation);
        AlprResults results;

        try {
            results = new AlprResults(stringResults);
        }
        catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        validateCzechPlates(results);
        filterNonLast(results);

        return results;
    }


    /**
     * Filter bad plates caused by blurred images - if the plates occurs in two frames in a row,
     * consider it as a valid plate.
     */
    private void filterNonLast(AlprResults results)
    {
        List<String> plates = new ArrayList<>();
        for(int i=0; i<results.getPlates().size(); i++)
        {
            String plate = results.getPlates().get(i).getBestPlate().getCharacters();
            plates.add(plate);

            if(!lastPlates.contains(plate))
            {
                results.getPlates().remove(i);
                i--;
            }
        }

        lastPlates = plates;
    }


    /**
     * Validate czech plates format manually, because OpenALPR config seems not working
     * (postprocess_regex_letters, postprocess_regex_numbers)
     */
    private void validateCzechPlates(AlprResults results)
    {
        for(int i=0; i<results.getPlates().size(); i++)
        {
            AlprPlate plate = results.getPlates().get(i).getBestPlate();
            String czechPlate = toCzechPlate(plate.getCharacters());

            if(czechPlate == null)
            {
                results.getPlates().remove(i);
                i--;
            }
            else
            {
                plate.setCharacters(czechPlate);
            }
        }
    }


    /**
     * Convert invalid letters to match czech pattern. If the conversion is ambigous, return null.
     */
    private String toCzechPlate(String plate)
    {
        // Allowed length
        if(plate.length() < 5 || plate.length() > 8)
            return null;

        // Replace 'O' to '0'
        plate = plate.replace('O', '0');

        // Forbidden letters
        if(plate.contains("G") || plate.contains("Q") || plate.contains("W"))
            return null;

        return plate;
    }

    //
    // Native methods
    //

    native private void startAlpr_native(String runtime_dir);
    native private String recognize_native(byte[] image, int len, int rotation);
}
