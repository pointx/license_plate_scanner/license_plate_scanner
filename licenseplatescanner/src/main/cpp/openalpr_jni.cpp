#include <jni.h>
#include <string>
#include <opencv2/core.hpp>
#include <android/log.h>
#include <memory>
#include <mutex>

#include "3rdparty/openalpr/src/openalpr/alpr.h"
#include "3rdparty/tesseract/api/baseapi.h"

#include "stdio_redirect.h"

#define  LOG_TAG    "openalpr_jni"

#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

// Global OpenALPR instance. According to OpenALPR docs, the instance is not thread-safe, so
// thread will firstly copy this pointer before scanning begins. When a new thread is created, a new
// instance must replace the old one in this variable.
std::shared_ptr<alpr::Alpr> alprInstance;


extern "C"
JNIEXPORT void JNICALL
Java_cz_pointx_licenseplatescanner_alpr_PlateRecognizer_startAlpr_1native(JNIEnv *env, jobject instance,
                                                                      jstring runtime_dir_)
{
    if(start_logger("SpzCameraReader") != 0)
        LOGE("Native log redirection failed!");

    const char * runtime_dir_ptr = env->GetStringUTFChars(runtime_dir_, 0);

    std::string runtime_dir(runtime_dir_ptr);
    std::string config_file = runtime_dir + "/openalpr.conf";

    // When this method is called, a new alpr::Alpr instance is created. If the old thread still
    // exists, during scanning it has a alpr::Alpr native instance which is not thread-safe. New
    // thread will create its own instance, so they do not collide.
    alprInstance = std::make_shared<alpr::Alpr>("eu", config_file, runtime_dir);

    env->ReleaseStringUTFChars(runtime_dir_, runtime_dir_ptr);
    LOGI("OpenALPR instance created");
}


extern "C"
JNIEXPORT jstring JNICALL
Java_cz_pointx_licenseplatescanner_alpr_PlateRecognizer_recognize_1native(JNIEnv *env,
                                                                      jobject instance,
                                                                      jbyteArray image_,
                                                                      jint len,
                                                                      jint rotation)
{
    // Copy pointer for case that a new thread (and also alpr::Alpr instance) is created to not
    // deallocate this one (in case of scanning in progress).
    std::shared_ptr<alpr::Alpr> alpr = alprInstance;

    // Get image to native bytes
    jbyte * image = env->GetByteArrayElements(image_, NULL);
    char * image_raw = reinterpret_cast<char *>(image);
    std::vector<char> img(len);
    for(int i=0; i<len; ++i)
        img[i] = image_raw[i];

    // Perform plate recognition
    alpr::AlprResults results = alpr->recognizeWithRotation(img, rotation);

    // Cleanup and post result
    env->ReleaseByteArrayElements(image_, image, 0);
    std::string json = alpr::Alpr::toJson(results);
    return env->NewStringUTF(json.c_str());
}