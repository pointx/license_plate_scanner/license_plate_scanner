package cz.pointx.licenseplatescannerdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Arrays;

import cz.pointx.licenseplatescanner.PlateScanningActivity;

public class DemoActivity extends Activity
{
    Button btnScan;
    TextView lblResult;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        btnScan = (Button)findViewById(R.id.btnScan);
        lblResult = (TextView)findViewById(R.id.lblResult);

        btnScan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DemoActivity.this, PlateScanningActivity.class);
                startActivityForResult(i, PlateScanningActivity.REQUEST_PLATE_SCAN);
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(data != null)
        {
            String[] platesArray = data.getStringArrayExtra(PlateScanningActivity.PLATES_RESULT);
            String plates = Arrays.toString(platesArray);
            lblResult.setText(plates);
        }
    }



    @Override
    protected void onResume()
    {
        super.onResume();
    }


    @Override
    protected void onPause()
    {
        super.onPause();
    }



}
